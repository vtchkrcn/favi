<?php

declare(strict_types = 1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
	/**
	 * @Route(path="/", name="index")
	 */
	public function index(): Response
	{
		 return $this->json(['message' => 'Hello world!']);
	}
}