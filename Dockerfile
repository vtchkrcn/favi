FROM php:7.4.12-apache

COPY config/docker/apache/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY config/docker/php/php.ini /usr/local/etc/php/php.ini

WORKDIR /var/www/html

RUN apt-get update -y \
    && apt-get install -y \
    git \
    gnupg2 \
    libpng-dev \
    && apt-get remove --purge -y javascript-common \
    && docker-php-ext-install \
    mysqli \
    pdo \
    pdo_mysql \
    calendar \
    gd \
    && docker-php-ext-enable pdo_mysql \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && pecl install xdebug-2.9.8 \
    && docker-php-ext-enable xdebug \
    && a2enmod rewrite \
    && service apache2 restart